# SmashCastGifsAPI
SmashCast API Documentation: [https://developers.smashcast.tv/#introduction](Link URL)

## Requirements
Request your manager to grand your account with access rights to SmashCast Private Specs repo (smashcast-cocoapods-specs.git).

## Installation

SmashCastGifsAPI is available through [CocoaPods](http://cocoapods.org). 

* Add the following lines on top of your Podfile:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
source 'git@bitbucket.org:azubutv/azubu_ios_cocoapods_specs.git'

...

pod "SmashCastGifsAPI"

```

## How to publish new version

**IMPORTANT:** Use 'develop' branch for development, not 'master'.

Before you can perform steps described below, you must add 'SmashCast-Specs' repository to your local machine. 
Use following Terminal command:
```shell 
pod repo add SmashCast-Specs git@bitbucket.org:smashcast/smashcast_cocoapods_specs.git
```
**IMPORTANT:** Be sure that you have access to SmashCast repositories and your prepared .ssl keys added to your bitbucket account.

1. Update pod version by modifying .podspec file placed inside project root directory.
2. Make sure that all frameworks and dependencies added to .podspec nor podfile.
3. Update description inside .podspec
4. Push all code to 'develop' branch and then merge 'develop' into 'master' with tag named according to version that specified inside .podspec
5. Now call you can push .podspec to private pod-specs repository by following command.

```shell
pod repo push --allow-warnings SmashCast-Specs SmashCastAPI.podspec
```

More details about private pod-spec: [https://guides.cocoapods.org/making/private-cocoapods.html](Link URL)

## Tests

To run tests, clone the repo, and run `pod install` from the Example directory first.
Then use **Xcode hot-key** `CMD + U` to launch UnitTests.

## Author
Vitaliy Grigorishyn, vgrigorishyn@geeksforless.net;
