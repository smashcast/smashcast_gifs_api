//
//  SCGAppDelegate.h
//  SmashCastGifsAPI
//
//  Created by vgrigorishyn on 06/13/2017.
//  Copyright (c) 2017 vgrigorishyn. All rights reserved.
//

@import UIKit;

@interface SCGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
