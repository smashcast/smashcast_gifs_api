//
//  main.m
//  SmashCastGifsAPI
//
//  Created by vgrigorishyn on 06/13/2017.
//  Copyright (c) 2017 vgrigorishyn. All rights reserved.
//

@import UIKit;
#import "SCGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCGAppDelegate class]));
    }
}
