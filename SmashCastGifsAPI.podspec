#
# Be sure to run `pod lib lint SmashCastGifsAPI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SmashCastGifsAPI'
  s.version          = '0.1.2'
  s.summary          = 'SmashCast GIFs Client for iOS'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
SmashCast GIFs Client. Provides ability to get gifs objects using Tenor GIFs API. https://tenor.com/gifapi#start
                       DESC

  s.homepage         = 'https://bitbucket.org/smashcast/smashcast_gifs_api'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vgrigorishyn' => 'vgrigorishyn@geeksforless.net' }
  s.source           = { :git => 'https://bitbucket.org/smashcast/smashcast_gifs_api.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  s.source_files = 'SmashCastGifsAPI/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SmashCastGifsAPI' => ['SmashCastGifsAPI/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'Foundation'
  s.dependency 'AFNetworking', '~> 3.0'
  s.dependency 'Typhoon'
  s.dependency 'EasyMapping', '~> 0.15'

end
