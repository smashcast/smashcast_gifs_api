//
//  SCGNetworkClientsAssemblyProtocol.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Foundation/Foundation.h>

@protocol SCGServiceConfigurationProtocol;

@protocol SCGServiceConfigurationAssemblyProtocol <NSObject>

- (id<SCGServiceConfigurationProtocol>)defaultServiceConfiguration;

@end
