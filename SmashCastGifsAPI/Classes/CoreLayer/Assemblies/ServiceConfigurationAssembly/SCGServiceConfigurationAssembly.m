//
//  NetworkClientsAssembly.m
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import "SCGServiceConfigurationAssembly.h"
#import "SCGServiceConfiguration.h"

static NSString *const kSCGServiceConfigurationDefaultKey = @"LIVDSRZULELA";
static NSString *const kSCGServiceConfigurationDefaultCountry = @"";
static NSString *const kSCGServiceConfigurationDefaultLocale = @"en_US";

@implementation SCGServiceConfigurationAssembly

#pragma mark - SCGServiceConfigurationAssemblyProtocol

- (id<SCGServiceConfigurationProtocol>)defaultServiceConfiguration {
    return [TyphoonDefinition withClass:[SCGServiceConfiguration class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithKey:country:locale:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:kSCGServiceConfigurationDefaultKey];
            [initializer injectParameterWith:kSCGServiceConfigurationDefaultCountry];
            [initializer injectParameterWith:kSCGServiceConfigurationDefaultLocale];
        }];
    }];
}

@end
