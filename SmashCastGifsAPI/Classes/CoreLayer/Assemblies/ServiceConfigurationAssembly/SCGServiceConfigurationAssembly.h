//
//  NetworkClientsAssembly.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Typhoon/Typhoon.h>
#import "SCGServiceConfigurationAssemblyProtocol.h"

@interface SCGServiceConfigurationAssembly : TyphoonAssembly <SCGServiceConfigurationAssemblyProtocol>

@end
