//
//  SCGNetworkClientsAssemblyProtocol.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Foundation/Foundation.h>

@protocol SCGNetworkClientProtocol;

@protocol SCGNetworkClientsAssemblyProtocol <NSObject>

- (id<SCGNetworkClientProtocol>)defaultNetworkClient;

@end
