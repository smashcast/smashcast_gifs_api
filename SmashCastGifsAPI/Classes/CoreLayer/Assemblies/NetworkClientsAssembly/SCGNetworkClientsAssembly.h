//
//  NetworkClientsAssembly.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Typhoon/Typhoon.h>
#import "SCGNetworkClientsAssemblyProtocol.h"

@interface SCGNetworkClientsAssembly : TyphoonAssembly <SCGNetworkClientsAssemblyProtocol>

@end
