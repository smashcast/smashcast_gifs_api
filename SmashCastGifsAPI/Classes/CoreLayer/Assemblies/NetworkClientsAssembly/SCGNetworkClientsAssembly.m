//
//  NetworkClientsAssembly.m
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import "SCGNetworkClientsAssembly.h"
#import "SCGDefaultNetworkClient.h"

static  NSString *const kSCGDefaultNetworkClientBaseUrl = @"https://api.tenor.co";
static  NSString *const kSCGDefaultNetworkClientAPIPath = @"/v1/";

@implementation SCGNetworkClientsAssembly

#pragma mark - SCGNetworkClientsAssemblyProtocol

- (id<SCGNetworkClientProtocol>)defaultNetworkClient {
    return [TyphoonDefinition withClass:[SCGDefaultNetworkClient class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithBaseURL:apiPath:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSURL URLWithString:kSCGDefaultNetworkClientBaseUrl]];
            [initializer injectParameterWith:kSCGDefaultNetworkClientAPIPath];
        }];
    }];
    
}

@end
