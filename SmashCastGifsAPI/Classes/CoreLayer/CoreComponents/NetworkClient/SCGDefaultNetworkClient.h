//
//  SCGNetworkService.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <Foundation/Foundation.h>
#import "SCGNetworkClientProtocol.h"

/**
 @author Vitaliy Grigorishyn
 
 A very simple network client which perfoms simple GET requests with params.
 */
@interface SCGDefaultNetworkClient : NSObject <SCGNetworkClientProtocol>

@property (nonatomic, copy, readonly) NSURL *baseURL;
@property (nonatomic, copy, readonly) NSString *apiPath;

/**
 @author Vitaliy Grigorishyn
 
 The main initializer of the SCGDefaultNetworkClient
 
 @param baseURL The base URL (e.g. https://myapi.com)
 @param apiPath The path to a server endpoint (e.g. /v1/rest/)
 
 @return SCGDefaultNetworkClient
 */
- (instancetype)initWithBaseURL:(NSURL *)baseURL
                        apiPath:(NSString *)apiPath;

@end
