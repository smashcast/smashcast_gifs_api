//
//  SCGNetworkService.m
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import "SCGDefaultNetworkClient.h"
#import <AFNetworking/AFNetworking.h>

@interface SCGDefaultNetworkClient ()

@property (strong, nonatomic) AFHTTPSessionManager *httpSessionManager;

@end

@implementation SCGDefaultNetworkClient

#pragma mark - Lifecycle

- (instancetype)initWithBaseURL:(NSURL *)baseURL apiPath:(NSString *)apiPath {
    self = [super init];
    if (self) {
        _baseURL = [baseURL copy];
        _apiPath = [apiPath copy];
        
        NSURL *fullBaseURL = [_baseURL URLByAppendingPathComponent:_apiPath];
        self.httpSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:fullBaseURL];
    }
    return self;
}

#pragma mark - SCGNetworkServiceProtocol

- (void)performGET:(NSString *)URLString
        parameters:(NSDictionary *)parameters
   completionBlock:(SCGNetworkClientCompletionBlock)completionBlock {
    [self.httpSessionManager GET:URLString
                      parameters:parameters
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                             completionBlock(responseObject, nil);
                         }
                         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                             completionBlock(nil, error);
                         }];
}

@end
