//
//  SCGNetworkService.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <Foundation/Foundation.h>

typedef void(^SCGNetworkClientCompletionBlock)(NSDictionary *response, NSError *error);

@protocol SCGNetworkClientProtocol <NSObject>

- (void)performGET:(NSString *)URLString
        parameters:(NSDictionary *)parameters
   completionBlock:(SCGNetworkClientCompletionBlock)completionBlock;

@end
