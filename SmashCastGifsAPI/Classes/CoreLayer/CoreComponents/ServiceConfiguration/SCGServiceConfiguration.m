//
//  SCGServiceConfiguration.m
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import "SCGServiceConfiguration.h"

@implementation SCGServiceConfiguration

#pragma mark - Lifecycle

- (instancetype)initWithKey:(NSString *)key
                    country:(NSString *)country
                     locale:(NSString *)locale {
    self = [super init];
    if (self) {
        self.key = key;
        self.country = country;
        self.locale = locale;
    }
    return self;
}

@end
