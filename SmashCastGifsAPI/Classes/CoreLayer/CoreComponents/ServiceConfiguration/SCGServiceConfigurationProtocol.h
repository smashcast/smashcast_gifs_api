//
//  SCGServiceConfigurationProtocol.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Foundation/Foundation.h>

@protocol SCGServiceConfigurationProtocol <NSObject>

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *locale;

@end
