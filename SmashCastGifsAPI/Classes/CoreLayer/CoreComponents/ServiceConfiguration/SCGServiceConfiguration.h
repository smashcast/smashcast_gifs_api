//
//  SCGServiceConfiguration.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Foundation/Foundation.h>
#import "SCGServiceConfigurationProtocol.h"

@interface SCGServiceConfiguration : NSObject <SCGServiceConfigurationProtocol>

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *locale;

- (instancetype)initWithKey:(NSString *)key
                    country:(NSString *)country
                     locale:(NSString *)locale;

@end
