//
//  SCGTag.m
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import "SCGTag.h"

@implementation SCGTag

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[SCGTag class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{
                                               @"searchterm" : NSStringFromSelector(@selector(searchTerm)),
                                               @"image" : NSStringFromSelector(@selector(imageURL)),
                                               @"name" : NSStringFromSelector(@selector(title)),
                                               }];
    }];
}

@end
