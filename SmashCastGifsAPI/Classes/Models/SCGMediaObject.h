//
//  SCGMediaObject.h
//  Pods
//
//  Created by vgrigorishyn on 6/15/17.
//
//

#import <EasyMapping/EasyMapping.h>

@interface SCGMediaObject : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSURL *previewURL;
@property (nonatomic, copy) NSURL *URL;
@property (nonatomic, assign) CGFloat size;

@end
