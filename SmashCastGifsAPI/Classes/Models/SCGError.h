//
//  SCGError.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <EasyMapping/EasyMapping.h>

@interface SCGError : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *error;

@end
