//
//  SCGGif.m
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import "SCGGif.h"

@implementation SCGGif

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[SCGGif class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{
                                               @"id" : NSStringFromSelector(@selector(gifID)),
                                               @"title" : NSStringFromSelector(@selector(title)),
                                               @"tags" : NSStringFromSelector(@selector(tags))
                                               }];
        
        [mapping mapKeyPath:@"created" toProperty:NSStringFromSelector(@selector(createdDate)) withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
            NSTimeInterval timeInterval = [value doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
            return date;
        }];
        
        [mapping mapKeyPath:@"media" toProperty:NSStringFromSelector(@selector(mediaObject)) withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
            if (![value isKindOfClass:[NSArray class]] || ![value count]) {
                return nil;
            }
            NSDictionary *tinyGifFromResponse = [[value objectAtIndex:0] objectForKey:@"tinygif"];
            SCGMediaObject *tinyGifMediaObject = [EKMapper objectFromExternalRepresentation:tinyGifFromResponse
                                                                                withMapping:[SCGMediaObject objectMapping]];
            NSLog(@"url: - %@", tinyGifMediaObject.URL.absoluteString);
            return tinyGifMediaObject;
        }];
    }];
}

@end
