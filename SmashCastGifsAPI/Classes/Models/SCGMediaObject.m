//
//  SCGMediaObject.m
//  Pods
//
//  Created by vgrigorishyn on 6/15/17.
//
//

#import "SCGMediaObject.h"

@implementation SCGMediaObject

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"preview" toProperty:NSStringFromSelector(@selector(previewURL))
             withValueBlock:[EKMappingBlocks urlMappingBlock]
               reverseBlock:[EKMappingBlocks urlReverseMappingBlock]];
        [mapping mapKeyPath:@"url" toProperty:NSStringFromSelector(@selector(URL))
             withValueBlock:[EKMappingBlocks urlMappingBlock]
               reverseBlock:[EKMappingBlocks urlReverseMappingBlock]];
        [mapping mapPropertiesFromArray:@[NSStringFromSelector(@selector(size))]];
    }];
}

@end
