//
//  SCGResponseObjectsModel.h
//  Pods
//
//  Created by vgrigorishyn on 6/22/17.
//
//

#import <EasyMapping/EasyMapping.h>
#import "SCGGif.h"

@interface SCGResponseObjectsModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray *gifObjects;

@property (nonatomic, copy) NSString *next;

@end
