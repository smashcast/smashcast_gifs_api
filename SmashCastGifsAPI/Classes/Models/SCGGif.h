//
//  SCGGif.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <EasyMapping/EasyMapping.h>
#import "SCGMediaObject.h"

@interface SCGGif : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger gifID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSDate *createdDate;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) SCGMediaObject *mediaObject;

@end
