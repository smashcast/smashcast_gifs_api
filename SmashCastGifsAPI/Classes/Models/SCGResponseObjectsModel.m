//
//  SCGResponseObjectsModel.m
//  Pods
//
//  Created by vgrigorishyn on 6/22/17.
//
//

#import "SCGResponseObjectsModel.h"

@implementation SCGResponseObjectsModel

+ (EKObjectMapping *)objectMapping {
    return [EKObjectMapping mappingForClass:[SCGResponseObjectsModel class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{
                                               @"next" : NSStringFromSelector(@selector(next))
                                               }];
        
        [mapping mapKeyPath:@"results" toProperty:NSStringFromSelector(@selector(gifObjects)) withValueBlock:^id _Nullable(NSString * _Nonnull key, id  _Nullable value) {
            NSArray *gifs = [EKMapper arrayOfObjectsFromExternalRepresentation:value
                                                                   withMapping:[SCGGif objectMapping]];
            return gifs;
        }];
    }];
}

@end
