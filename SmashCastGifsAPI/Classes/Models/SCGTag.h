//
//  SCGTag.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <EasyMapping/EasyMapping.h>

@interface SCGTag : NSObject

@property (nonatomic, copy) NSString *searchTerm;
@property (nonatomic, copy) NSURL *imageURL;
@property (nonatomic, copy) NSString *title;

@end
