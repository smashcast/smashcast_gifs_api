//
//  SCGServiceAssembly.m
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import "SCGServiceAssembly.h"
#import "SCGService.h"
#import "SCGNetworkClientsAssembly.h"
#import "SCGServiceConfigurationAssembly.h"

@implementation SCGServiceAssembly

#pragma mark -  SCGServiceAssemblyProtocol

- (id<SCGServiceProtocol>)defaultSmashCastGifsService {
    return [TyphoonDefinition withClass:[SCGService class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithConfiguration:andNetworkClient:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self.serviceConfigurationAssembly defaultServiceConfiguration]];
            [initializer injectParameterWith:[self.networkClientsAssembly defaultNetworkClient]];
        }];
    }];
}

@end
