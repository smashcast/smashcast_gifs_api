//
//  SCGServiceAssembly.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Typhoon/Typhoon.h>
#import "SCGServiceAssemblyProtocol.h"

@class SCGNetworkClientsAssembly;
@class SCGServiceConfigurationAssembly;

@interface SCGServiceAssembly : TyphoonAssembly <SCGServiceAssemblyProtocol>

@property (strong, nonatomic) SCGNetworkClientsAssembly *networkClientsAssembly;
@property (strong, nonatomic) SCGServiceConfigurationAssembly *serviceConfigurationAssembly;

@end
