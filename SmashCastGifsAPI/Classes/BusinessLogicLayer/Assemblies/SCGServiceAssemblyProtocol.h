//
//  SCGServiceAssemblyProtocol.h
//  Pods
//
//  Created by vgrigorishyn on 6/14/17.
//
//

#import <Foundation/Foundation.h>

@protocol SCGServiceProtocol;

@protocol SCGServiceAssemblyProtocol <NSObject>

- (id<SCGServiceProtocol>)defaultSmashCastGifsService;

@end
