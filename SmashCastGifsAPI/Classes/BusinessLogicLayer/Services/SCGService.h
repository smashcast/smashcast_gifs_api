//
//  SCGService.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <Foundation/Foundation.h>
#import "SCGServiceProtocol.h"
    
@protocol SCGNetworkClientProtocol;
@protocol SCGServiceConfigurationProtocol;

@interface SCGService : NSObject <SCGServiceProtocol>

@property (nonatomic, strong) id<SCGNetworkClientProtocol> networkClient;
@property (nonatomic, strong) id<SCGServiceConfigurationProtocol> configuration;

- (instancetype)initWithConfiguration:(id<SCGServiceConfigurationProtocol>)configuration
                     andNetworkClient:(id<SCGNetworkClientProtocol>)networkClient;

@end
