//
//  SCGServiceProtocol.h
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import <Foundation/Foundation.h>

@class SCGGif;
@class SCGTag;
@class SCGError;

typedef void(^SCGGifsCompletionBlock)(NSArray<SCGGif *> * gifs, NSString *next, SCGError *error);
typedef void(^SCGTagsCompletionBlock)(NSArray<SCGTag *> * tags, SCGError *error);

@protocol SCGServiceProtocol <NSObject>

- (void)fetchTagsWithCompletionBlock:(SCGTagsCompletionBlock)completionBlock;

- (void)fetchTrendingGifsWithPosition:(NSString *)position
                                limit:(NSInteger)limit
                           completion:(SCGGifsCompletionBlock)completionBlock;

- (void)performSearchGifsWithTag:(SCGTag *)tag
                        position:(NSInteger)position
                           limit:(NSInteger)limit
                      completion:(SCGGifsCompletionBlock)completionBlock;

@end
