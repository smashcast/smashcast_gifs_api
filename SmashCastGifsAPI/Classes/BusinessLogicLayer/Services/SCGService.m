//
//  SCGService.m
//  Pods
//
//  Created by vgrigorishyn on 6/13/17.
//
//

#import "SCGService.h"
#import "SCGNetworkClientProtocol.h"
#import "SCGServiceConfigurationProtocol.h"
#import "SCGGif.h"
#import "SCGTag.h"
#import "SCGError.h"
#import "SCGResponseObjectsModel.h"

NSString * const SCGServiceTags = @"tags";
NSString * const SCGServiceTrending = @"trending";
NSString * const SCGServiceSearch = @"search";

@implementation SCGService

#pragma mark - Lifecycle

- (instancetype)initWithConfiguration:(id<SCGServiceConfigurationProtocol>)configuration
                     andNetworkClient:(id<SCGNetworkClientProtocol>)networkClient {
    self = [super init];
    if (self) {
        self.configuration = configuration;
        self.networkClient = networkClient;
    }
    return self;
}

#pragma mark - Protocol conformance
#pragma mark SCGServiceProtocol

- (void)fetchTagsWithCompletionBlock:(SCGTagsCompletionBlock)completionBlock {
}

- (void)fetchTrendingGifsWithPosition:(NSString *)position
                                limit:(NSInteger)limit
                           completion:(SCGGifsCompletionBlock)completionBlock {
    NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionary];
    if (position && ![position isEqualToString:@""]) {
        mutableParameters[@"pos"] = position;
    }
    if (limit >= 0) {
        mutableParameters[@"limit"] = @(limit);
    }
    mutableParameters[@"key"] = self.configuration.key;
    
    [self.networkClient performGET:SCGServiceTrending parameters:mutableParameters completionBlock:^(NSDictionary *response, NSError *error) {
        NSString *next = [response objectForKey:@"next"];
        NSArray *gifsFromResponse = [response objectForKey:@"results"];
        NSArray *gifs = [EKMapper arrayOfObjectsFromExternalRepresentation:gifsFromResponse
                                                               withMapping:[SCGGif objectMapping]];
        completionBlock(gifs, next, nil);
    }];
}

- (void)performSearchGifsWithTag:(SCGTag *)tag
                        position:(NSInteger)position
                           limit:(NSInteger)limit
                      completion:(SCGGifsCompletionBlock)completionBlock {
}

@end
